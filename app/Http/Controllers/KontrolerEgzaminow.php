<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class KontrolerEgzaminow extends Controller
{
    public function ee09_40(){
      return view('strony.egzaminy.EE09-40pyt');
    }

    public function ee09_1(){
        return view('strony.egzaminy.EE09-1pyt');
    }
}