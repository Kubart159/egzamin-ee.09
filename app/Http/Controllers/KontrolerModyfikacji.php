<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class KontrolerModyfikacji extends Controller
{
    public function dodawanie(){
        return view('strony.modyfikacja.dodawanie')->with('podtytul','dodawanie');  
    }
    public function lista(){
        return view('strony.modyfikacja.lista')->with('podtytul','lista');  
    }
    public function modyfikacja(){
        return view('strony.modyfikacja.modyfikacja')->with('podtytul','modyfikacja');  
    }
}
