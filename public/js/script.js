var poprawna = "";
var wybranaOdp = "";

function wybor(x, y){
    wybranaOdp = x;
    poprawna =y;
    if(poprawna==wybranaOdp){
        poprawnaOdpowiedz();
    }else{
        blednaOdpowiedz();
    }
    
}

function poprawnaOdpowiedz() {
    zaznaczaniePoprawnej();
    wyswietlanieButtonu();
}

function blednaOdpowiedz() {
    zaznaczaniePoprawnej();
    zaznaczanieBlednej();
    wyswietlanieButtonu();
}

function zaznaczaniePoprawnej() {
    $("#"+poprawna).css("background-color","#2d9909");  
}

function zaznaczanieBlednej(){
    $("#"+wybranaOdp).css("background-color","#990909");
}

function wyswietlanieButtonu(){
    $('#nextButton').css("display","block");
}

function nastepne(){
    location.reload();
    return false;
}