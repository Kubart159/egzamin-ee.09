-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 10 Gru 2020, 12:22
-- Wersja serwera: 10.1.32-MariaDB
-- Wersja PHP: 7.0.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `egzamin`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `pytania`
--

CREATE TABLE `pytania` (
  `nr_pyt` int(11) NOT NULL,
  `pytanie` text COLLATE utf8_polish_ci NOT NULL,
  `odpA` text COLLATE utf8_polish_ci NOT NULL,
  `odpB` text COLLATE utf8_polish_ci NOT NULL,
  `odpC` text COLLATE utf8_polish_ci NOT NULL,
  `odpD` text COLLATE utf8_polish_ci NOT NULL,
  `pop` varchar(1) COLLATE utf8_polish_ci NOT NULL,
  `kategoria` varchar(20) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `pytania`
--

INSERT INTO `pytania` (`nr_pyt`, `pytanie`, `odpA`, `odpB`, `odpC`, `odpD`, `pop`, `kategoria`) VALUES
(1, 'W podanej regule CSS: h1 {color: blue} h1 oznacza', 'deklarację', 'selektor', 'klasę', 'wartość', 'B', 'CSS'),
(2, 'Model opisu przestrzeni barw o parametrach: odcień, nasycenie i jasność, to ', 'CMYK', 'CMY', 'RGB', 'HSV', 'D', 'HTML'),
(3, 'Który parametr obiektu graficznego ulegnie zmianie po modyfikacji wartości kanału alfa? ', 'Nasycenie barw', 'Przezroczystość', 'Kolejność wyświetlania pikseli', 'Ostrość krawędzi', 'B', 'HTML'),
(4, 'Jak nazywa się proces przedstawienia, we właściwej dla danego środowiska formie, informacji zawartej w dokumencie elektronicznym?', 'Teksturowanie', 'Mapowanie', 'Rasteryzacja', 'Renderowanie', 'D', 'INNE'),
(5, 'Jakie sa nazwy typowych poleceń języka zapytań SQL, związane z wykonywaniem operacji na danych SQL DML (np.: umieszczanie danych w bazie, kasowanie dokonywanie zmian w danych)? ', 'DENY, GRANT, REVOKE ', 'DELETE, INSERT, UPDATE ', 'SELECT, SELECT INTO ', 'ALTER, CREATE, DROP ', 'B', 'SQL'),
(6, 'Jakiego typu specjalizowane oprogramowanie narzędziowe należy zainstalować, aby umożliwić wykonywanie jego użytkownikowi operacji na zgromadzonych danych? ', 'Klucz obcy', 'System Zarządzania Bazą Danych (SZBD) ', 'Otwarty mechanizm komunikacji bazy danych ', 'Obiektowy System Zarządzania Bazą Danych ', 'B', 'SQL'),
(7, 'Co należy zastosować w organizacji danych, aby zapytania w bazie danych były wykonywane szybciej?', 'Wartości domyślne', 'Indeksy', 'Reguły', 'Klucze podstawowe', 'B', 'SQL'),
(8, 'Jakie należy posiadać uprawnienia, aby wykonać i odtworzyć kopię zapasową bazy danych Microsoft SQL Server 2005 Express?', 'Users', 'Security users', 'Administrator systemu', 'Użytkownik lokalny', 'C', 'SQL'),
(9, 'Zmienne typu int odnoszą się do liczb ', 'w notacji zmiennoprzecinkowej', 'w notacji stałoprzecinkowej', 'naturalnych', 'całkowitych', 'D', 'Javascript'),
(11, 'W palecie kolorów RGB kolor żółty jest połączeniem dwóch kolorów: zielonego i czerwonego. Który z kodów szesnastkowych oznacza kolor żółty? ', '#00FFFF', '#FF00FF', '#F0F0F0', '#FFFF00', 'D', 'HTML'),
(12, 'Który z formatów zapewnia największa redukcję rozmiaru pliku dźwiękowego?', 'WAV', 'CD-Audio', 'MP3', 'PCM', 'C', 'INNE'),
(13, 'Którego ze słów kluczowych języka SQL należy użyć, aby wyeliminować duplikaty?', 'ORDER BY', 'LIKE', 'GROUP BY', 'DISTINCT', 'D', 'SQL'),
(14, 'Które ze stwierdzeń prawidłowo charakteryzuje zdefiniowaną tabelę: CREATE TABLE dane (kolumna INTEGER(3));', 'Tabela posiada jedną kolumnę zawierającą trzy elementowe tablice', 'Tabela o nazwie dane posiada jedną kolumnę liczb całkowitych', 'Kolumny tabeli dane nazywają się: kolumna1, kolumna2, kolumna3', 'Tabela o nazwie dane posiada trzy kolumny liczb całkowitych', 'B', 'SQL'),
(15, 'Które polecenie wydane z konsoli systemu operacyjnego, zawierające w swojej składni opcję --repair, umożliwia naprawę bazy danych? ', 'create', 'mysqlcheck', 'truncate', 'mysqldump', 'B', 'SQL'),
(16, 'Która z wymienionych funkcji sortowania wykorzystywana w języku PHP sortuje tablicę asocjacyjną według indeksów ', 'ksort()', 'sort()', 'rsort()', 'asort()', 'A', 'PHP'),
(18, 'W językach programowania tylko zmienna jednego typu wbudowanego może przyjmować wyłącznie dwie wartości. Jest to typ ', 'tablicowy', 'logiczny', 'znakowy', 'łańcuchowy', 'B', 'Javascript'),
(19, 'Instrukcja języka PHP tworząca obiekt pkt dla zdefiniowanej w ramce klasy Punkt ma postać', 'pkt Punkt;', 'Punkt() pkt;', 'pkt Punkt();', 'pkt = new Punkt();', 'D', 'PHP'),
(20, 'Poprzez deklarację var x=\"true\"; w języku JavieScript tworzy się zmienną typu ', 'Logicznego', 'Nieokreślonego (undefined)', 'Liczbowego', 'String (ciąg znaków)', 'D', 'Javascript'),
(21, 'Parametr face znacznika <font> służy do określenia', 'wielkości czcionki', 'barwy czcionki', 'nazwy czcionki', 'efektów czcionki', 'C', 'CSS'),
(22, 'Kaskadowe arkusze stylów tworzy się w celu', 'definiowania sposobu formatowania elementów strony internetowej', 'uzupełnienia strony internetowej o treści tekstowe', 'przyspieszenia wyświetlania grafiki na stronie internetowej', 'ułatwienia użytkownikowi nawigacji', 'A', 'CSS'),
(23, 'Które polecenie w CSS służy do załączenia zewnętrznego arkusza stylów?', 'import', 'open', 'require', 'include', 'A', 'CSS'),
(24, 'Selektor CSS a:link {color:red} zawarty w kaskadowych arkuszach stylów definiuje ', 'identyfikator', 'pseudoklasę', 'klasę', 'pseudoelement', 'B', 'CSS'),
(25, 'Jak nazywa się edytor wspomagający tworzenie stron internetowych, którego sposób działania można w polskim tłumaczeniu określić jako: otrzymujesz to, co widzisz? ', 'IDE', 'VISUAL EDITOR', 'WEB STUDIO', 'WYSIWYG', 'D', 'HTML'),
(26, 'Saturacja koloru nazywana jest inaczej', 'nasyceniem koloru', 'przezroczystością koloru', 'jasnością koloru', 'dopełnieniem koloru', 'A', 'CSS'),
(27, 'Operator arytmetyczny modulo w języku SQL to', '%', '&', '||', '/', 'A', 'SQL'),
(28, 'Polecenie w języku SQL ALTER TABLE USA... ma za zadanie', 'utworzenie nowej tabeli USA', 'modyfikację tabeli USA', 'nadpisanie starej tabeli USA', 'usunięcie tabeli USA', 'B', 'SQL'),
(29, 'Kod: SELECT imie, pesel, wiek FROM dane WHERE wiek IN (18,30) spowoduje wybranie: ', 'imion, nazwisk i numerów PESEL osób w wieku poniżej 18 lat ', 'imion, numerów PESEL i wieku osób z przedziału od 18 do 30 lat ', 'imion, numerów PESEL i wieku osób w wieku równym 18 lub 30 lat ', 'imion, numerów PESEL i wieku osób posiadających powyżej 30 lat ', 'C', 'SQL'),
(30, 'Aby policzyć wszystkie wiersze tabeli Koty należy użyć polecenia: ', 'SELECT COUNT(Koty) AS ROWNUM ', 'SELECT ROWNUM() FROM Koty ', 'SELECT COUNT(ROWNUM) FROM Koty ', 'SELECT COUNT(*) FROM Koty ', 'D', 'SQL'),
(31, 'W algebrze relacji operacja selekcji polega na', 'wyelminowaniu pustych wierszy', 'wyelminowaniu krotek z powtarzającymi się polami', 'wybraniu krotek spełniających określone warunki', 'wybraniu krotek niezawierających wartości NULL', 'C', 'SQL'),
(32, 'Wskaż poprawną kolejność etapów projektowania relacyjnej bazy danych', 'Selekcja, Określenie relacji, Określenie kluczy podstawowych tabel, Określenie zbioru danych', 'Określenie relacji, Określenie kluczy podstawowych tabel, Selekcja, Określenie zbioru danych', 'Określenie zbioru danych, Selekcja, Określenie kluczy podstawowych tabel, Określenie relacji', 'Określenie kluczy podstawowych tabel, Określenie zbioru danych, Selekcja, Określenie relacji', 'C', 'SQL'),
(33, 'Integralność referencyjna w modelu relacyjnych baz danych oznacza, że', 'wartość klucza głównego oraz klucza obcego nie jest pusta', 'każdemu kluczowi głównemu odpowiada dokładnie jeden klucz obcy w tabeli lub tabelach powiązanych', 'klucz główny lub klucz obcy nie zawierają wartości NULL', 'wartość klucza obcego w danej tabeli musi być albo równa wartości klucza głównego w tabeli z nia powiązanej albo równa wartości NULL', 'D', 'SQL'),
(34, 'Obiekt typu array w języku Javascript służy do przechowywania', 'wielu wartości lub funkcji', 'wielu wartości wyłącznie tekstowych', 'wielu wartości wyłącznie liczbowych', 'wielu wartości dowolnego typu', 'D', 'Javascript'),
(35, 'W języku PHP zmienna $_GET jest zmienną', 'zwykłą, zdefiniowaną przez twórcę strony', 'predefiniowaną, używaną do przekazywania danych do skryptów PHP poprzez adres strony', 'zdefiniowaną przez twórcę strony, służącą do przekazywania danych z formularza przez adres strony', 'predefiniowaną, używaną do gromadzenia wartości formularza po nagłówkach zlecenia HTTP (danych z formularza nie można zobaczyć w adresie)', 'B', 'PHP'),
(36, 'W języku PHP w instrukcji switch musi występować', 'przynajmniej dwie instrukcje case', 'instrukcja default', 'konstrukcja switch(wyrażenie)', 'instrukcja break po każdej instrukcji case', 'C', 'PHP'),
(37, 'Konstruktor w języku PHP jest metodą o nazwie ', '_open', '_create', '_contruct', '_new', 'C', 'PHP'),
(38, 'Który fragment kodu JavaScript zwróci wartość true? ', '„def” > „abc”', '„abc” > „def”', '„a” > „b”', '„ab” > „c”', 'A', 'Javascript'),
(39, 'W celu określenia wysokości obrazka wyświetlonego na stronie WWW należy wykorzystać właściwość CSS o nazwie', 'margin', 'width', 'height', 'padding', 'C', 'CSS'),
(40, 'Aby ustawić czcionkę Verdana w kodzie CSS, należy wykorzystać właściwość ', 'font-family: Verdana;', 'font-name: Verdana;', 'font-style: Verdana;', 'font-weight: Verdana;', 'A', 'CSS'),
(41, 'Kwerenda pozwalająca na wprowadzenie zmian w wielu rekordach lub przeniesienie wielu rekordów przy użyciu pojedynczej operacji, nosi nazwę kwerendy ', 'parametrycznej', 'krzyżowej', 'funkcjonalnej', 'wybierającej', 'C', 'SQL'),
(42, 'Które z poleceń naprawi uszkodzoną tabelę w języku SQL? ', 'ANALYZE TABLE tbl_name ', 'REGENERATE TABLE tbl_name ', 'REPAIR TABLE tblname ', 'OPTIMIZE TABLE tbl_name ', 'C', 'SQL'),
(43, 'Który zapis stylu CSS ustawi tło bloku na kolor niebieski?', 'div {border-color: blue;}', 'div {color: blue;}', 'div {shadow: blue;}', 'div {background-color: blue;}', 'D', 'CSS'),
(44, 'Domyślna nazwa pliku konfiguracyjnego serwera Apache to', 'configuration.php', '.htaccess', '.configuration', 'htaccess.cnf', 'B', 'PHP'),
(45, 'Organizacja zajmująca się ustalaniem standardu dla języka HTML nosi nazwę ', 'ISO', 'W3C', 'NASK', 'WYSIWYG', 'B', 'HTML'),
(46, 'Która ze zdefiniowanych funkcji w języku PHP jako wynik zwraca połowę kwadratu wartości przekazanej?', 'function licz($a) { echo $a*$a/2; }', 'function licz($a) { return $a/2; }', 'function licz($a) { return $a*$a/2; }', 'function licz($a) { echo $a/2; }', 'C', 'PHP'),
(47, 'Projektowanie logicznego układu witryny polega na', 'zdefiniowaniu treści witryny', 'ustaleniu adresów URL dla podstron witryny', 'rozmieszczeniu elementów w konkretnych miejscach witryny', 'opracowaniu zestawu grafik dla witryny', 'C', 'HTML'),
(48, 'Prosta animacja może być zapisana w formacie', 'BMP', 'GIF', 'PSD', 'TIFF', 'B', 'CSS'),
(49, 'Która z instrukcji umożliwia wysłanie tekstu do przeglądarki? ', 'exit', 'break', 'type', 'echo', 'D', 'PHP'),
(50, 'Aby stworzyć tabelę w bazie danych, należy zastosować polecenie SQL ', 'NEW TABLE', 'CREATE TABLE', 'PLUS TABLE', 'ADD TABLE', 'B', 'SQL'),
(51, 'W skład typowego frameworka wchodzą', 'domena i obsługa błędów', 'mechanizm uruchamiania i przetwarzania akcji, oraz certyfikat http', 'zarządzanie komunikacją z bazą danych, mechanizm uruchamiania i przetwarzania akcji', 'obsługa formularzy i wbudowany serwer', 'C', 'PHP'),
(52, 'W języku skryptowym JavaScript zmienne mogą być deklarowane', 'tylko jeśli podamy typ zmiennej i jej nazwę', 'zawsze z poprzedzającym nazwę znakiem $', 'tylko na początku skryptu', 'w momencie pierwszego użycia zmiennej', 'D', 'Javascript'),
(53, 'W językach programowania zmienna typu integer służy do przechowywania ', 'wartości logicznej', 'liczby całkowitej', 'znaku', 'liczby rzeczywistej', 'B', 'Javascript'),
(54, 'Wskaż dwa sposoby zabezpieczenia bazy danych Microsoft Access', 'Ustalenie zabezpieczeń na poziomie użytkownika oraz sesji', 'Zaszyfrowanie pliku bazy danych oraz SMSy z kodem autoryzującym', 'Funkcje anonimowe oraz ustalenie hasła otwarcia bazy danych', 'Ustalanie hasła do otwarcia bazy danych oraz zabezpieczeń na poziomie użytkownika', 'D', 'SQL'),
(55, 'Certyfikat SSL jest stosowany do ', 'deszyfracji transmitowanych danych ', 'blokowania szkodliwego oprogramowania w witrynie ', 'zidentyfikowania właściciela domeny ', 'zapisania danych o sesjach tworzonych w witrynie ', 'C', 'HTML'),
(56, 'W języku SQL przywilej SELECT polecenia GRANT pozwala użytkownikowi baz danych na ', 'odczyt danych z tabeli', 'modyfikowanie danych w tabeli ', 'usunięcie danych z tabeli ', 'tworzenie tabeli ', 'A', 'SQL'),
(57, 'Polecenie SQL o treści: UPDATE artykuly SET cena = cena * 0.7 WHERE kod = 2; oznacza', 'w tabeli artykuly obniża wartość każdego pola cena dla którego pole kod jest równe 2', 'w tabeli artykuly obniża wartość każdego pola cena o 30% dla wszystkich artykułów', 'wprowadzenie w tabeli artykuly pola o nazwie cena ze znacznikiem kod', 'wprowadzenie w tabeli artykuly nowych pól cena i kod', 'A', 'SQL'),
(58, 'Do edycji grafiki wektorowej stosuje się program', 'Audacity', 'Wordpad', 'Corel Draw', 'Paint', 'C', 'INNE'),
(61, 'W języku CSS właściwość font-size przyjmuje, według słów kluczowych, wartości', 'wyłączenie small, medium, large', 'jedynie small, smaller,large, larger', 'tylko big i small', 'ze zbioru xx-small, x-small, medium, large, x-large, xx-large', 'D', 'CSS'),
(62, 'Prawidłowy, zgodny ze standardem języka XHTML, zapis samozamykającego się znacznika odpowiadającego za łamanie linii ma postać', '<br />', '</br/>', '</ br>', '<br> </br>', 'A', 'HTML'),
(63, 'Najprostszą i najmniej pracochłonną metodą przetestowania działania witryny internetowej w wielu przeglądarkach i ich różnych wersjach jest', 'skorzystanie z emulatora przeglądarek internetowych np. Browser Sandbox', 'skorzystanie z walidatora języka HTML', 'skorzystanie z walidatora języka HTML', 'testowanie witryny w programie Internet Explorer, zakładając kompatybilność innych przeglądarek', 'A', 'SQL'),
(64, 'Aby przenieść witrynę na serwer, można skorzystać z oprogramowania', 'FileZilla', 'CloneZilla', 'Bugzilla', 'Go!Zilla', 'A', 'HTML'),
(65, 'Aby utworzyć relację jeden do wielu, w tabeli po stronie wiele, należy zdefiniować', 'klucz obcy wskazujący na klucz podstawowy tabeli po stronie jeden', 'klucz sztuczny odnoszący się do kluczy podstawowych obu tabel', 'klucz podstawowy wskazujący na klucz podstawowy tabeli po stronie jeden', ' klucz obcy wskazujący na klucz obcy tabeli po stronie jeden', 'A', 'SQL'),
(66, 'Baza danych 6-letniej szkoły podstawowej zawiera tabelę szkola z polami: imie, nazwisko, klasa. Wszyscy uczniowie klas 1-5 zdali do następnej klasy. Aby zwiększyć wartość w polu klasa o 1 należy użyć polecenia', 'UPDATE nazwisko, imie SET klasa=klasa+1 WHERE klasa>1 OR klasa<5;', 'UPDATE szkola SET klasa=klasa+1 WHERE klasa>=1 AND klasa <=5;', 'SELECT nazwisko, imie FROM klasa=klasa+1 WHERE klasa>1 OR klasa <5;', 'SELECT szkola FROM klasa=klasa+1 WHERE klasa >=1 AND klasa <=5;', 'B', 'SQL'),
(67, 'Przed wykonaniem kopii bezpieczeństwa bazy danych, tak aby kopia ta była poprawna i możliwa do późniejszego odtworzenia, należy sprawdzić ', 'możliwość udostępnienia bazy danych ', 'spójność bazy danych ', 'poprawność składni zapytań ', 'prawa dostępu do serwera bazy danych ', 'B', 'SQL'),
(68, 'W MS SQL Server polecenie RESTORE DATABASE służy do ', 'odtworzenia bazy danych z kopii bezpieczeństwa ', 'usunięcia bazy danych z serwera centralnego subskrybenta', 'przebudowania bazy danych w oparciu o buforowane dane ', 'odświeżenia bazy danych z kontrolą więzów integralności ', 'A', 'SQL'),
(69, 'W języku JavaScript poprawnie nadana zmienna to ', '#imie', 'imię2', 'imie2', 'imię%', 'C', 'Javascript'),
(70, 'Wykonanie kodu JavaScript w przeglądarce wymaga', 'debugowania', 'zamiany na kod maszynowy', 'kompilowania', 'interpretowania', 'D', 'Javascript'),
(71, 'Kod strony WWW napisanej w języku PHP', 'jest wykonywany po stronie serwera', 'jest wykonywany po stronie klienta', 'może być uruchomiony bez obsługi serwera WWW', 'jest przetwarzany na tych samych zasadach co JavaScript', 'A', 'PHP'),
(72, 'Polecenie pg_connect języka PHP służy do połączenia z bazą', 'PostgreSQL', 'MS ACCESS', 'MS SQL', 'mySQL', 'A', 'PHP'),
(73, 'Program debugger służy do:', 'tłumaczenia kodu zapisanego językiem wyższego poziomu na język maszynowy', 'analizy kodu źródłowego w celu odnalezienia błędów składniowych', 'analizy wykonywanego programu w celu lokalizacji błędów', 'interpretacji kodu w wirtualnej maszynie Java', 'C', 'INNE'),
(74, 'Którego języka należy użyć, aby zapisać skrypt wykonywany po stronie klienta w przegladarce internetowej?', 'JavaScript', 'Perl', 'PHP', 'Python', 'A', 'Javascript'),
(75, 'W języku PHP pobrano z bazy danych wyniki działania kwerendy za pomocą polecenia mysql_query(). Aby otrzymać ze zwróconej kwerendy wierszy danych, należy zastosować polecenie:', 'mysql_list_fields()', 'mysql_field_len()', 'mysql_fetch_lengths()', 'mysql_fetch_row()', 'D', 'PHP'),
(76, 'Błędy interpretacji kodu PHP są zapisane:', 'w podglądzie zdarzeń systemu Windows', 'w oknie edytora, w którym powstaje kod PHP', 'nigdzie, są ignorowanie przez przeglądarkę oraz interpreter kodu PHP', 'w logu pod warunkiem ustawienia odpowiedniego parametru w pliku php.ini', 'D', 'PHP'),
(77, 'Wartość i typ zmiennej w języku PHP można sprawdzić za pomocą funkcji', 'readfile()', 'implode()', 'var_dump()', 'strlen()', 'C', 'PHP'),
(78, 'Do uruchomienia systemu CMS Joomla! wymagane jest środowisko:', 'IIS, PERL i MySQL', 'PHP i MySQL', 'Apache, PHP i MySQL', 'Apache i PHP', 'C', 'INNE'),
(79, 'Znacznik <i> języka HTML służy do', 'umieszczenia obrazka', 'zdefiniowania formularza', 'zdefiniowania nagłówka w tekście', 'zmiany kroju pisma na pochylony', 'D', 'HTML'),
(82, 'Chcąc zdefiniować formatowanie tabeli w języku CSS w taki sposób, aby wiersz, który jest aktualnie wskazywany kursorem myszy, został wyróżniony np. innym kolorem, należy zastosować ', 'pseudoelement :first-line ', 'pseudoklasę :visited ', 'pseudoklasę :hover ', 'nowy selektor klasy dla wiersza tabeli ', 'C', 'CSS'),
(83, 'Blok deklaracji postaci background-attachment: scroll powoduje, że', 'grafika tła będzie wyświetlona w prawym górnym rogu strony', 'tło strony będzie przewijane razem z tekstem', 'tło strony będzie stałe, a tekst będzie się przewijał', 'grafika tła będzie powtarzana (kafelki)', 'B', 'CSS'),
(84, 'Który język skryptowy ogólnego przeznaczenia należy wykorzystać do tworzenia aplikacji WWW, zagnieżdżanych w języku HTML i uruchamianych po stronie serwera?', 'JavaScript', 'Perl', 'PHP', 'C#', 'C', 'PHP'),
(86, 'Formatem zapisu rastrowych plików graficznych z kompresją bezstratną jest', 'CDR', 'PNG', 'JNG', 'SVG', 'B', 'HTML'),
(87, 'W języku SQL klauzula DISTINCT instrukcji SELECT sprawi, że zwrócone dane', 'będą spełniały określony warunek', 'będą pogrupowane według określonego pola', 'nie będą zawierały powtórzeń', 'zostaną posortowane', 'C', 'SQL'),
(88, 'Zdefiniowano bazę danych z tabelą podzespoły o polach: model, producent, typ, cena. Aby wyświetlić wszystkie modele pamięci RAM firmy Kingston w kolejności od najtańszej do najdroższej, należy posłużyć się kwerendą: ', 'SELECT model FROM podzespoly WHERE typ=\"RAM\" AND producent=\"Kingston\" ORDER BY cena DESC; ', 'SELECT model FROM producent WHERE typ=\"RAM\" OR producent=\"Kingston\" ORDER BY podzespoly ASC; ', 'SELECT model FROM podzespoly WHERE typ=\"RAM\" OR producent=\"Kingston\" ORDER BY cena DESC; ', 'SELECT model FROM podzespoly WHERE typ=\"RAM\" AND producent=\"Kingston\" ORDER BY cena ASC; ', 'D', 'SQL'),
(89, 'W celu przyspieszenia operacji na bazie danych należy do pól często wyszukiwanych lub sortowanych', 'dodać więzy integralności', 'stworzyć osobną tabelę przechowującą tylko te pola', 'dodać klucz obcy', 'utworzyć indeks', 'D', 'SQL'),
(90, 'Jednoznacznym identyfikatorem rekordu w bazie danych jest pole', 'klucza podstawowego', 'numeryczne', 'relacji', 'klucza obcego', 'A', 'SQL'),
(91, 'Jednoznacznym identyfikatorem rekordu w bazie danych jest pole', 'relacji', 'numeryczne', 'klucza obcego', 'klucza podstawowego', 'D', 'SQL'),
(92, 'W bazie danych sklepu istnieje tabela artykuly zawierająca pole o nazwie nowy. Aby to pole wypełnić wartościami TRUE dla każdego rekordu, należy zastosować kwerendę', 'INSERT INTO artykuly VALUE nowy=TRUE;', 'INSERT INTO nowy FROM artykuly SET TRUE;', 'UPDATE nowy FROM artykuly VALUE TRUE;', 'UPDATE artykuly SET nowy=TRUE;', 'D', 'SQL'),
(93, 'Za pomocą polecenia BACKUP LOG w MS SQL Server można ', 'wykonać pełną kopię bezpieczeństwa ', 'wykonać kopię bezpieczeństwa dziennika transakcyjnego ', 'zalogować sie do kopii bezpieczeństwa ', 'przeczytać komunikaty wygenerowane podczas tworzenia kopii ', 'B', 'SQL'),
(94, 'Polecenie DBCC CHECKDB(\"sklepAGD\", Repair_fast) w MS SQL Server', 'sprawdzi spójność określonej tabeli i naprawi uszkodzone rekordy', 'sprawdzi spójność określonej tabeli', ' sprawdzi spójność bazy danych i wykona kopię bezpieczeństwa', 'sprawdzi spójność bazy danych i naprawi uszkodzone indeksy', 'A', 'SQL'),
(95, 'Aby zdefiniować łamanie linii tekstu, np. w zmiennej napisowej, należy posłużyć się znakiem ', 'slash', 'n', 'b', 't', 'B', 'Javascript'),
(96, 'Interpreter PHP wygeneruje błąd i nie wykona kodu, jeżeli programista:', 'będzie deklarował zmienne wewnątrz warunku', 'pobierze wartość z formularza, w którym pole input nie było wypełnione', 'nie postawi średnika po wyrażeniu w instrukcji if, jeśli po nim nastąpiła sekcja else', 'będzie pisał kod bez wcięć', 'C', 'PHP'),
(101, 'W języku CSS, aby formatować tekst poprzez przekreślenie, podkreślenie dolne lub górne, należy zastosować', 'text-indent', 'text-align', 'text-decoration', 'text-transform', 'C', 'CSS'),
(103, 'W czasie przetwarzania dźwięku, aby pozbyć się niechcianych odgłosów spowodowanych złą jakością mikrofonu, należy zastosować narzędzie', 'usuwania szumów', 'echa', 'obwiedni', 'wyciszenia', 'A', 'INNE'),
(104, 'Dana jest tabela psy o polach: imie, rasa, telefon_wlasciciela, rok_szczepienia. Aby wyszukać telefony właścicieli, których psy były szczepione przed 2015 rokiem, należy użyć polecenia SQL', 'SELECT imie, rasa FROM psy WHERE rok_szczepienia > 2015', 'SELECT telefon_wlasciciela FROM psy WHERE rok_szczepienia < 2015', 'SELECT psy FROM rok_szczepienia < 2015', 'SELECT telefon_wlasciciela FROM psy WHERE rok_szczepienia > 2015', 'B', 'SQL'),
(106, 'W JavaScript wynik operacji jest równy wartości NaN, jeśli skrypt próbował wykonać ', 'działanie arytmetyczne, a zawartość zmiennej była napisem ', 'funkcję sprawdzającą długość napisu, a zawartość zmiennej była liczbą ', 'funkcję parseFloat zamiast parseInt na zmiennej liczbowej ', 'działanie arytmetyczne na dwóch zmiennych liczbowych dodatnich ', 'A', 'Javascript'),
(107, 'W instrukcji warunkowej języka JavaScript należy sprawdzić przypadek, gdy wartość zmiennej a jest z przedziału (0, 100), natomiast wartość zmiennej b jest większa od zera. Warunek taki jest prawidłowo zapisany w nastepujący sposób', 'if ((a>0 || a<100) && b>0)', 'if (a>0 || a<100 || b<0)', 'if ((a>0 && a<100) || b<0)', 'if (a>0 && a<100 && b>0)', 'D', 'Javascript'),
(108, 'Aby wykonać kod zapisany językiem PHP wystarczy, że w systemie zainstalowano', 'serwer WWW z serwerem MySQL', 'przeglądarkę internetową', 'serwer WWW z interpreterem PHP', 'serwer WWW, parser PHP oraz serwer MySQL', 'C', 'PHP'),
(109, 'Zadaniem funkcji PHP o nazwie mysql_num_rows() jest', 'zwrócić kolejny rekord z wynikami zapytania', 'zwrócić liczbę wierszy znajdujących się w wyniku zapytania', 'zwrócić rekord, którego numer podany został w parametrze funkcji', 'ponumerować rekordy w bazie danych', 'B', 'PHP'),
(110, 'Testy aplikacji internetowej mające za zadanie sprawdzenie skalowalności aplikacji i bazy danych oraz architektury serwera i konfiguracji noszą nazwę testów', 'kompatybilności', 'użyteczności', 'bezpieczeństwa', 'funkcjonalnych', 'A', 'SQL'),
(111, 'W języku CSS, aby zdefiniować niestandardowe odstępy między wyrazami, stosuje się właściwość', 'line-spacing', 'letter-space', 'word-spacing', 'white-space', 'C', 'CSS'),
(112, 'Kolor zapisany kodem RGB o wartości rgb(128, 16, 8) w postaci szesnastkowej ma wartość', '#800F80', '#FF1008', '#FF0F80', '#801008', 'D', 'CSS'),
(113, 'Aby zbadać rozkład ilościowy poszczególnych kolorów zdjęcia, należy użyć ', 'rozmycia Gaussa', 'desaturacji', 'balansu kolorów', 'histogramu', 'D', 'HTML'),
(114, 'Dana jest tabela programiści o polach: id, nick, ilosc_kodu, ocena. Pole ilosc_kodu zawiera liczbę linii kodu napisanych przez programistę w danym miesiącu. Aby policzyć sumę linii kodu, który napisali wszyscy programiści, należy użyć polecenia', 'SELECT SUM(ocena) FROM ilosc_kodu;', 'SELECT SUM(ilosc_kodu) FROM programisci;', 'SELECT MAX(ilosc_kodu) FROM programisci', 'SELECT COUNT(programisci) FROM ilosc_kodu;', 'B', 'HTML'),
(115, 'W instrukcji CREATE TABLE użycie klauzuli PRIMARY KEY przy deklaracji pola tabeli spowoduje, że pole to stanie się', 'indeksem klucza', 'indeksem unikalnym', 'kluczem podstawowym', 'kluczem obcym', 'C', 'SQL'),
(116, 'Baza danych księgarni zawiera tabelę ksiazki z polami: id, idAutor, tytul, ileSprzedanych oraz tabelę autorzy z polami: id, imie, nazwisko. Aby stworzyć raport sprzedanych książek z tytułami i nazwiskami autorów, należy', 'Zdefiniować relację 1..1 dla tabel ksiazki i autorzy, a następnie stworzyć kwerendę łączącą obie tabele', 'Zdefiniować relację 1..n dla tabel ksiazki i autorzy, a następnie stworzyć kwerendę łączącą obie tabele', 'stworzyć dwie osobne kwerendy: pierwszą wyszukującą tytuły książek, drugą wyszukującą nazwiska autorów', 'stworzyć kwerendę wyszukującą tytuły książek', 'B', 'SQL'),
(117, 'Baza danych MySQL uległa uszkodzeniu. Które z działań NIE pomoże przy jej naprawie?', 'Próba naprawy poleceniem REPAIR', 'Stworzenie nowej bazy i przeniesienie do niej tabel', 'Odtworzenie bazy z kopii bezpieczeństwa', 'Wykonanie replikacji bazy danych', 'D', 'SQL'),
(119, 'Do uruchomienia skryptu JavaScript wymagane jest oprogramowanie ', 'przeglądarki internetowej ', 'serwera MySQL ', 'serwera WWW ', 'debugera JavaScript ', 'A', 'Javascript'),
(122, 'W języku CSS wcięcie pierwszej linii akapitu na 30 pikseli uzyska się za pomocą zapisu ', 'p { line-height: 30px; } ', 'p { text-spacing: 30px; } ', 'p { line-indent: 30px; } ', 'p { text-indent: 30px; } ', 'D', 'CSS'),
(123, 'W języku CSS, aby sformatować dowolny element języka HTML w ten sposób, że po najechaniu na niego kursorem zmienia on kolor czcionki, należy zastosować pseudoklasę ', ':active', ':hover', ':coursor', ':visited', 'B', 'CSS'),
(124, 'Dana jest tabela pracownicy. Polecenie MySQL usuwające wszystkie rekordy z tabeli, dla których nie wypełniono pola rodzaj_umowy, ma postać', 'DROP pracownicy FROM rodzaj_umowy = 0;', 'DELETE pracownicy WHERE rodzaj_umowy = \'brak\';', 'DELETE FROM pracownicy WHERE rodzaj_umowy IS NULL;', 'DROP pracownicy WHERE rodzaj_umowy IS NULL;', 'C', 'SQL'),
(125, 'W przedstawionym fragmencie kwerendy języka SQL, komenda SELECT ma za zadanie zwrócić', 'sumę w kolumnie wartosc', 'średnią tabeli', 'średnią w kolumnie wartosc', 'liczbę wierszy', 'D', 'SQL'),
(126, 'Tabela filmy zawiera klucz główny id oraz klucz obcy rezyserID. Tabela rezyserzy zawiera klucz główny id. Obydwie tabele połączone są relacją jeden po stronie rezyserzy do wielu po stronie filmy. Aby w kwerendzie SELECT połączyć tabele filmy i rezyserzy, należy zapisać', '... filmy JOIN rezyserzy ON filmy.rezyserID = rezyserzy.filmyID ...', '... filmy JOIN rezyserzy ON filmy.rezyserID = rezyserzy.id ...', '... filmy JOIN rezyserzy ON filmy.id = rezyserzy.id ...', '... filmy JOIN rezyserzy ON filmy.id = rezyserzy.filmyID ...', 'B', 'SQL'),
(127, 'W tabeli podzespoly należy zmienić wartość pola URL na \'toshiba.pl\' dla wszystkich rekordów, gdzie pole producent to TOSHIBA. W języku SQL modyfikacja będzie miała postać', 'UPDATE podzespoly SET URL=\'toshiba.pl\';', 'UPDATE producent=\'TOSHIBA\' SET URL=\'toshiba.pl\';', 'UPDATE podzespoly.producent=\'TOSHIBA\' SET URL=\'toshiba.pl\';', 'UPDATE podzespoly SET URL=\'toshiba.pl\' WHERE producent=\'TOSHIBA\';', 'D', 'SQL'),
(128, 'W języku MySQL należy zastosować polecenie REVOKE, aby użytkownikowi anna odebrać prawo do dokonywania zmian jedynie w definicji struktury bazy danych. Polecenie odpowiadające odebraniu tych praw ma postać', 'REVOKE CREATE INSERT DELETE ON tabela1 FROM \'anna\'@\'localhost\'', 'REVOKE CREATE ALTER DROP ON tabela1 FROM \'anna\'@\'localhost\'', 'REVOKE CREATE UPDATE DROP ON tabela1 FROM \'anna\'@\'localhost\'', 'REVOKE ALL ON tabela1 FROM \'anna\'@\'localhost\'', 'B', 'SQL'),
(129, 'Polecenie DBCC CHECKDB(\"sklepAGD\", Repair_fast) w MS SQL Server', 'sprawdzi spójność określonej tabeli i naprawi uszkodzone rekordy', 'sprawdzi spójność bazy danych i wykona kopię bezpieczeństwa', 'sprawdzi spójność określonej tabeli', 'sprawdzi spójność bazy danych i naprawi uszkodzone indeksy', 'D', 'SQL'),
(130, 'W języku PHP funkcja trim ma za zadanie ', 'Porównywać dwa napisy i wypisać część wspólną ', 'Usuwać białe znaki lub inne znaki podane w parametrze, z obu końców napisu ', 'Podawać długość napisu ', 'Zmniejszać napis o wskazaną w parametrze liczbę znaków ', 'B', 'PHP'),
(131, 'Za pomocą języka PHP nie jest możliwe ', 'Generowanie dynamicznej zawartości strony ', 'Zmienianie dynamiczne zawartości strony HTML w przeglądarce ', 'Przetwarzanie danych formularzy ', 'Przetwarzanie danych zgromadzonych w bazie danych ', 'B', 'PHP'),
(132, 'W języku PHP, wykonując operacje na bazie danych MySQL, aby zakończyć pracę z bazą, należy wywołać ', 'mysqli_close();', 'mysqli_exit();', 'mysqli_rollback();', 'mysqli_commit();', 'A', 'PHP'),
(133, 'W języku CSS wartości underline, overline, blink przyjmują atrybut ', 'font-weight', 'font-style', 'text-style', 'text-decoration', 'D', 'CSS'),
(134, 'Aby edytować nakładające się na siebie pojedyncze fragmenty obrazu, pozostawiając pozostałe elementy niezmienione, należy zastosować ', 'Kadrowanie', 'Histogram', 'Warstwy', 'Kanał alfa', 'C', 'HTML'),
(135, 'Dana jest tabela o nazwie przedmioty z polami: ocena i uczenID. Aby policzyć średnią ocen ucznia o ID równym 7, należy posłużyć się zapytaniem', 'SELECT COUNT(ocena) FROM przedmioty WHERE uczenID = 7;', 'COUNT SELECT ocena FROM przedmioty WHERE uczenID = 7;', 'AVG SELECT ocena FROM przedmioty WHERE uczenID = 7;', 'SELECT AVG(ocena) FROM przedmioty WHERE uczenID = 7;', 'D', 'SQL'),
(136, 'Tabela o nazwie naprawy zawiera pola: klient, czyNaprawione. Aby usunąć te rekordy, w których pole czyNaprawione jest prawdą, należy posłużyć się poleceniem', 'DELETE klient FROM naprawy WHERE czyNaprawione = TRUE;', 'DELETE FROM naprawy WHERE czyNaprawione = TRUE;', 'DELETE FROM naprawy;', 'DELETE naprawy WHERE czyNaprawione = TRUE;', 'B', 'SQL'),
(137, 'Za pomocą polecenia ALTER TABLE można', 'usunąć rekord', 'zmienić wartości rekordów', 'zmienić strukturę tabeli', 'usunąć tabelę', 'C', 'SQL'),
(138, 'Wskaż zapisany w języku JavaScript warunek, który ma sprawdzić spełnienie przynajmniej jednego z przypadków: 1) dowolna naturalna liczba a jest trzycyfrowa, 2) dowolna całkowita liczba b jest ujemna ', '((a>99) || (a<1000)) && (b<0) ', '((a>99) || (a<1000)) || (b<0) ', '((a>99) && (a<1000)) || (b<0) ', '((a>99) && (a<1000)) && (b<0) ', 'C', 'Javascript'),
(139, 'Hermetyzacja to zasada programowania obiektowego mówiąca o tym, że', 'klasy/obiekty mogą współdzielić ze sobą funkcjonalność', 'pola i metody wykorzystywane tylko przez daną klasę/obiekt są ograniczone zasięgiem private lub protected', 'typy pól w klasach/obiektach mogą być dynamicznie zmieniane w zależności od danych im przypisywanych', 'klasy/obiekty mogą mieć zdefiniowane metody wirtualne, które są implementowane w pochodnych klasach/obiektach', 'B', 'Javascript'),
(140, 'W języku JavaScript, aby zmienić wartość atrybutu znacznika HTML, po uzyskaniu obiektu za pomocą metody getElementById należy skorzystać z ', 'pola innerHTML', 'metody getAttribute', 'pola attribute i podać nazwę atrybutu', 'metody setAttribute', 'D', 'Javascript'),
(142, 'W języku HTML atrybut shape znacznika area, określający typ obszaru, może przyjąć wartość', 'rect, poly, circle', 'poly, square, circle', 'rect, triangle, circle', 'rect, square, circle', 'A', 'HTML'),
(143, 'Wskaż prawidłową kolejność stylów CSS mając na uwadze ich pierwszeństwo w formatowaniu elementów strony WWW.', 'Wewnętrzny, Zewnętrzny, Rozciąganie stylu', 'Rozciąganie stylu, Zewnętrzny, Lokalny', 'Lokalny, Wewnętrzny, Zewnętrzny', 'Zewnętrzny, Wydzielone bloki, Lokalny', 'C', 'CSS'),
(144, 'Chcąc zdefiniować marginesy wewnętrzne dla danych: margines górny 50px, dolny 40px, prawy 20px i lewy 30px należy użyć składni CSS', 'padding: 50px, 20px, 40px, 30px;', 'padding: 40px, 30px, 50px, 20px;', 'padding: 50px, 40px, 20px, 30px;', 'padding: 20px, 40px, 30px, 50px;', 'A', 'CSS'),
(145, 'Który z atrybutów background-attachment w języku CSS należy wybrać, aby tło strony było nieruchome względem okna przeglądarki? ', 'Inherit', 'Scroll', 'Fixed', 'Local', 'C', 'CSS'),
(146, 'Połączenie dwóch barw leżących po przeciwnych stronach w kole barw jest połączeniem ', 'monochromatycznym', 'sąsiadującym', 'dopełniającym', 'trójkątnym', 'C', 'HTML'),
(147, 'Model barw oparty na 3 parametrach: odcień, nasycenie i jasność to ', 'CMY', 'CMYK', 'RGB', 'HSV', 'D', 'HTML'),
(148, 'Który z wymienionych formatów plików NIE JEST wykorzystywany do publikacji grafiki lub animacji na stronach internetowych? ', 'PNG', 'AIFF', 'SVG', 'SWF', 'B', 'HTML'),
(149, 'Pierwszym krokiem podczas przetwarzania sygnału analogowego na cyfrowy jest', 'kodowanie', 'kwantyzacja', 'filtrowanie', 'próbkowanie', 'D', 'INNE'),
(150, 'W języku SQL w wyniku wykonania zapytania ALTER TABLE osoba DROP COLUMN grupa; zostanie', 'zmieniona nazwa tabeli na grupa', 'zmieniona nazwa kolumny na grupa', 'dodana kolumna grupa', 'usunięta kolumna grupa', 'D', 'SQL'),
(151, 'W języku SQL w wyniku wykonania zapytania ALTER TABLE osoba DROP COLUMN grupa; zostanie', 'zmieniona nazwa tabeli na grupa', 'zmieniona nazwa kolumny na grupa', 'dodana kolumna grupa', 'dodana kolumna grupa', 'D', 'SQL'),
(152, 'Właściwym zestawem kroków według kolejności, które należy wykonać w celu nawiązania współpracy między aplikacją internetową po stronie serwera a bazą SQL, jest', 'wybór bazy danych, nawiązanie połączenia z serwerem baz danych, zapytanie do bazy, wyświetlenie na stronie WWW, zamknięcie połączenia', 'wybór bazy, zapytanie do bazy, nawiązanie połączenia z serwerem baz danych, wyświetlenie na stronie WWW, zamknięcie połączenia', 'nawiązanie połączenia z serwerem baz danych, wybór bazy, zapytanie do bazy - wyświetlane na stronie WWW, zamknięcie połączenia', 'zapytanie do bazy, wybór bazy, wyświetlenie na stronie WWW, zamknięcie połączenia', 'C', 'PHP'),
(153, 'Do poprawnego i spójnego działania bazy danych niezbędne jest umieszczenie w każdej tabeli', 'klucza obcego z wartością NOT NULL i UNIQUE', 'klucza FOREIGN KEY z wartością NOT NULL', 'klucza PRIMARY KEY z wartością NOT NULL i UNIQUE', 'kluczy PRIMARY KEY i FOREIGN KEY', 'D', 'SQL'),
(154, 'Wartość i typ zmiennej w języku PHP można sprawdzić za pomocą funkcji ', 'implode()', 'readfile()', 'strlen()', 'var_dump()', 'D', 'PHP'),
(155, 'Wskaż BŁĘDNY opis optymalizacji kodu wynikowego programu', 'Jej celem jest poprawienie wydajności programu', 'Jej celem jest sprawdzenie zgodności z wymogami formalnymi', 'Powinna prowadzić do modyfikacji kodu źródłowego do postaci, w której będzie on działał szybciej', 'W celu zwiększenia szybkości wykonywania kodu przez procesor może być prowadzona na różnych etapach pracy', 'B', 'Javascript'),
(156, 'W celu zmodyfikowania tekstu \"ala ma psa\" na \"ALA MA PSA\" należy użyć funkcji PHP', 'ucfirst(\"ala ma psa\");', 'strtolower(\"ala ma psa\");', 'strtoupper(\"ala ma psa\");', 'strstr(\"ala ma psa\");', 'C', 'PHP'),
(157, 'Poprawne udokumentowanie wzorca weryfikacji pola nazwa w części kodu aplikacji JavaScript to', '/* Pole nazwa może zawierać dowolny ciąg cyfr (z wyłączeniem 0), następnie musi zawierać dużą literę i ciąg minimum dwóch małych liter. */', '/* Pole nazwa musi składać się w kolejności: z ciągu cyfr (z wyłączeniem 0), następnie dużej litery i dwóch małych liter. */', '/* Pole nazwa może składać się z dowolnego ciągu cyfr (z wyłączeniem 0), małych i dużych liter. */', '/* Pole nazwa powinno składać się w kolejności: z ciągu cyfr (z wyłączeniem 0), następnie dużej litery i ciągu małych liter. */', 'A', 'Javascript'),
(159, 'Jak zdefiniować w języku CSS formatowanie hiperłącza, żeby nieodwiedzony link był w kolorze żółtym, a odwiedzony w kolorze zielonym? ', 'a:link {color: yellow;} a:visited{color: green;} ', 'a:hover {color: green;} a:link{color: yellow;} ', 'a:hover {color: yellow;} a:visited{color: green;} ', 'a:visited {color: yellow;} a:link{color: green;} ', 'A', 'CSS'),
(162, 'Polecenie SQL, które usuwa bazę danych o nazwie firma, ma postać ', 'DROP firma;', 'ALTER firma DROP;', 'ALTER firma DROP DATABASE;', 'DROP DATABASE firma;', 'D', 'SQL'),
(163, 'Polecenie SQL, które usuwa bazę danych o nazwie firma, ma postać', 'DROP firma', 'ALTER firma DROP', 'DROP DATABASE firma', 'ALTER firma DROP DATABASE', 'C', 'SQL'),
(164, 'Zastosowanie kwerendy SQL: DELETE FROM mieszkania WHERE status=1; spowoduje usunięcie', 'tabel, w których pole status jest równe 1, z bazy danych mieszkania', 'pola o nazwie status z tabeli mieszkania', 'tabeli mieszkania z bazy danych', 'rekordów, w których pole status jest równe 1, z tabeli mieszkania', 'D', 'SQL'),
(165, 'W języku JavaScript wynik działania instrukcji zmienna++; będzie taki sam jak instrukcji', 'zmienna=zmienna+10;', 'zmienna--;', 'zmienna+=1;', 'zmienna===zmienna+1;', 'C', 'Javascript'),
(166, 'Język JavaScrypt ma obsługę', 'funkcji wirtualnych', 'klas abstrakcyjnych', 'wysyłania ciastek z tą samą informacją do wielu klientów strony', 'obiektów DOM', 'D', 'Javascript'),
(167, 'Który opis odnosi się do metody POST wysyłania formularza? ', 'Dane przesyłane są za pomocą adresu URL, czyli w sposób widoczny dla użytkownika ', 'Posiada dodatkowe ograniczenia jakim jest długość adresu - maksymalnie 255 znaków ', 'Może być zapisana jako zakładka w przeglądarce internetowej ', 'Jest wskazana, gdy przesyłane są informacje poufne, np. hasło, numer telefonu czy numer karty kredytowej ', 'D', 'PHP'),
(168, 'Atrybut value w polu formularza XHTML', 'wskazuje odpowiedź domyślną', 'ustawia pole tylko do odczytu', 'ogranicza długość pola', 'wskazuje na nazwę pola', 'A', 'HTML'),
(171, ' linia kreskowana w stylu obramowania CSS jest określona właściwością', 'dotted', 'solid', 'dashed', 'double', 'C', 'CSS'),
(172, 'Kolor zapisany w notacji heksadecymalnej #0000FF to', 'zielony', 'czarny', 'niebieski', 'czerwony', 'C', 'CSS'),
(174, 'Które polecenie SQL zamieni w tabeli tab w kolumnie kol wartość Ania na Zosia?', 'ALTER TABLE tab CHANGE kol=\'Ania\' kol=\'Zosia\';', 'UPDATE tab SET kol=\'Ania\' WHERE kol=\'Zosia\';', 'UPDATE tab SET kol=\'Zosia\' WHERE kol=\'Ania\';', 'ALTER TABLE tab CHANGE kol=\'Zosia\' kol=\'Ania\';', 'C', 'SQL'),
(175, 'W języku PHP sumę logiczną oznacza operator ', '!', '+', '||', '&&', 'C', 'PHP'),
(176, 'W JavaScript wywołanie zdarzenia onKeydown nastąpi wtedy, gdy klawisz', 'myszki został zwolniony', 'myszki został naciśnięty', 'klawiatury został zwolniony', 'klawiatury został naciśnięty', 'D', 'Javascript'),
(179, 'W języku HTML dla hiperłącza wartość atrybutu target, która odpowiada za załadowanie strony do nowego okna lub karty, to', '_parent', '_top', '_blank', '_self', 'C', 'HTML'),
(181, 'Chcąc dodać do listy rozwijalnej przedstawionego formularza HTML możliwość zaznaczenia kilku opcji jednocześnie, należy w znaczniku select dodać atrybut', 'disabled', 'multiple', 'size', 'value', 'B', 'HTML'),
(182, 'Dla akapitu zdefiniowano styl CSS. Które właściwości stylu CSS poprawnie opisują dla akapitu krój czcionki: Arial; rozmiar czcionki: 16 pt; styl czcionki: pochylenie?', 'p {font-family: Arial; font-size: 16px; font-variant: normal;}', 'p {font-style: Arial; font-size: 16pt; font-variant: normal;}', 'p {font-style: Arial; size: 16px; font-weight: normal;}', 'p {font-family: Arial; font-size: 16pt; font-style: italic;}', 'D', 'CSS'),
(183, 'W CSS symbolem jednostki miary, wyrażonej w punktach edytorskich, jest ', 'in', 'em', 'pt', 'px', 'C', 'CSS'),
(184, 'Transformację w stylach CSS, polegającą na zamianie tylko pierwszych liter wszystkich wyrazów na wielkie, otrzymamy stosując polecenie', 'underline', 'capitalize', 'lowercase', 'uppercase', 'B', 'CSS'),
(185, 'Jednostka ppi (pixels per inch) ', 'Jednostka ppi (pixels per inch) ', 'jest parametrem określającym rozdzielczość cyfrowych urządzeń wykonujących pomiary ', 'określa rozdzielczość obrazów rastrowych ', 'określa rozdzielczości obrazów generowanych przez drukarki i plotery ', 'C', 'HTML'),
(186, 'W języku zapytań SQL, aby dodać do tabeli Towar kolumnę rozmiar typu znakowego o maksymalnej długości 20 znaków, należy wykonać polecenie ', 'ALTER TABLE Towar DROP COLUMN rozmiar varchar(20); ', 'ALTER TABLE Towar CREATE COLUMN rozmiar varchar(20); ', 'ALTER TABLE Towar ADD rozmiar varchar(20); ', 'ALTER TABLE Towar ALTER COLUMN rozmiar varchar(20); ', 'C', 'SQL'),
(187, 'W języku skryptowym JavaScript operatory: ||, && należą do grupy operatorów ', 'przypisania', 'arytmetycznych', 'logicznych', 'bitowych', 'C', 'Javascript'),
(188, 'Liczba 0x142, zapisana w kodzie skryptu JavaScript, ma postać', 'dwójkową', 'szesnastkową', 'ósemkową', 'dziesiętną', 'B', 'Javascript'),
(189, 'Wskaż pętlę, która w języku JavaScript wyświetli sześć kolejnych liczb parzystych ', 'for(i=2;i<=12;i+=2) {document.write(i);} ', 'for(i=2;i<=12;i++) {i=i+2; document.write(i);} ', 'for(i=2;i<12;i+=2) {document.write(i);} ', 'for(i=2;i<12;i++) {i++; document.write(i);} ', 'A', 'Javascript'),
(190, 'Wybierz poprawną definicję funkcji w języku JavaScript ', 'new nazwa_funkcji(argumenty) {instrukcje;} ', 'function nazwa_funkcji(argumenty) {instrukcje;} ', 'nazwa_funkcji(argumenty) {instrukcje;} ', 'typ_funkcji nazwa_funkcji(argumenty) {instrukcje;} ', 'B', 'Javascript'),
(191, 'W języku PHP zapisywanie danych do pliku realizuje funkcja', 'freadfile()', 'fopen()', 'fputs()', 'fgets()', 'C', 'PHP'),
(192, 'Plikiem konfiguracyjnym, który pozwala na zdefiniowanie ustawień PHP dla całego serwera, jest', 'httpd.conf', 'config.inc.php', 'my.ini', 'php.ini', 'D', 'PHP'),
(194, 'W języku JavaScript, funkcja matematyczna Math.pow() służy do wyznaczenia', 'wartości bezwzględnej liczby', 'pierwiastka kwadratowego liczby', 'wartości przybliżonej liczby', 'potęgi liczby', 'D', 'Javascript'),
(198, 'W języku CSS przypisano regułę: float:left; dla bloku. Reguła ta zostanie wykorzystana do ', 'wyrównanie elementów tabeli do lewej strony ', 'wyrównania tekstu do lewej strony ', 'ustawienia bloku na lewo względem innych ', 'ustawienia bloków jeden pod drugim ', 'C', 'CSS'),
(199, 'W języku CSS wartości: underline, overline, line-through oraz blink dotyczą właściwości', 'text-align', 'text-decoration', 'font-decoration', 'font-style', 'B', 'CSS'),
(200, 'W kodzie HTML kolor biały można zapisać za pomocą wartości', '#255255', 'rgb(255,255,255)', 'rgb(FFFF,FF)', '#000000', 'B', 'HTML'),
(201, 'Grafik wykonał logo strony internetowej. To czarny znaczek na przezroczystym tle. Aby zachować wszystkie atrybuty obrazu i umieścić go na stronie internetowej, grafik powinien zapisać obraz w formacie', 'BMP', 'JPG', 'CDR', 'PNG', 'D', 'HTML'),
(202, 'W języku SQL polecenie ALTER TABLE ma za zadanie ', 'modyfikować kolumny w tabeli ', 'dodawać tabelę do bazy danych ', 'usuwać tabelę z bazy danych ', 'modyfikować dane rekordów w tabeli ', 'A', 'SQL'),
(203, 'Które wyrażenie logiczne należy zastosować w języku JavaScript, aby wykonać operacje tylko dla dowolnych liczb ujemnych z przedziału jednostronnie domkniętego <-200,-100)?', '(liczba >=-200) && (liczba<-100)', '(liczba <=-200) && (liczba<-100)', '(liczba <=-200) || (liczba>-100)', '(liczba >=-200) || (liczba>-100)', 'A', 'Javascript'),
(205, 'W języku JavaScript zdefiniowano obiekt Samochod. Aby wywołać jedną z metod tego obiektu, należy zapisać', 'Samochod.spalanie_na100', 'Samochod.kolor', 'Samochod.spalanie()', 'Samochod.()', 'C', 'Javascript'),
(207, 'Do optymalnej realizacji algorytmu szukającego największej z trzech podanych liczb a, b i c, wystarczy zastosować', 'dwie tablice', 'jedną pętlę', 'pięć zmiennych', 'dwa warunki', 'D', 'Javascript'),
(208, 'W języku JavaScript metoda getElementById() odwołuje się do ', 'znacznika HTML  o podanym id', 'zmiennej liczbowej', 'klasy zdefiniowanej w CSS', 'znacznika HTML  o podanej nazwie klasy', 'A', 'Javascript'),
(211, 'Klucz obcy w tabeli jest tworzony po to, aby ', 'umożliwić jednoznaczną identyfikację rekordu w tabeli ', 'łączyć go z innymi kluczami obcymi tabeli ', 'zdefiniować relację 1..n wiążącą go z kluczem głównym innej tabeli ', 'stworzyć formularz wpisujący dane do tabeli ', 'C', 'SQL'),
(212, 'Które ze stwierdzeń dotyczących klucza podstawowego jest prawdziwe?', 'Może przyjmować tylko wartości liczbowe', 'Jest unikalny w obrębie tabeli', 'Składa się tylko z jednego pola', 'Dla tabeli z danymi osobowymi może być to pole nazwisko', 'B', 'SQL'),
(213, 'W języku SQL aby zmodyfikować dane w tabeli, należy posłużyć się poleceniem', 'CREATE', 'JOIN', 'UPDATE', 'SELECT', 'C', 'SQL'),
(214, 'W wyniku połączenia relacją kluczy głównych dwóch tabel otrzymuje się relację typu', 'jeden do jednego', 'jeden do wielu', 'wiele do jednego', 'wiele do wielu', 'A', 'SQL'),
(215, 'Obiektem służącym w bazie danych do podsumowywania, wyświetlania i wydruków danych jest', 'zapytanie', 'zestawienie', 'raport', 'formularz', 'C', 'SQL'),
(216, 'W bazie danych zdefiniowano tabelę Mieszkancy wypełnioną danymi. Aby usunąć tę tabelę wraz z zawartością, należy posłużyć się poleceniem ', 'TRUNCATE TABLE Mieszkancy;', 'DROP TABLE Mieszkancy;', 'DELETE FROM Mieszkancy;', 'ALTER TABLE Mieszkancy;', 'B', 'SQL'),
(217, 'Aby odebrać uprawnienia użytkownikowi, należy zastosować polecenie ', 'DELETE PRIVILEGES', 'GRANT NO PRIVILEGES', 'REVOKE', 'DELETE', 'C', 'SQL'),
(218, 'Aby aplikacja PHP mogła komunikować się z bazą danych, niezbędne jest w pierwszej kolejności wywołanie funkcji o nazwie ', 'mysqli_connect', 'mysqli_select_db', 'mysqli_create_db', 'mysqli_close', 'A', 'PHP'),
(220, 'Język HTML dysponuje nagłówkami do budowania hierarchii treści. Nagłówki te występują jedynie w zakresie', 'h1 - h10', 'h1 - h6', 'h1 - h4', 'h1 - h8', 'B', 'HTML'),
(221, 'Aby zdefiniować krój czcionki w stylu CSS, należy użyć właściwości', 'font-style', 'text-style', 'text-family', 'font-family', 'D', 'CSS'),
(222, 'Aby obraz umieszczony na stronie internetowej automatycznie skalował się do rozmiaru ekranu, na którym jest wyświetlana strona, należy', 'nie modyfikować obu jego wymiarów stylami CSS', 'oba jego wymiary ustawić w pikselach', 'jego szerokość ustawić w wartościach procentowych', 'jeden z jego wymiarów ustawić w pikselach', 'C', 'CSS'),
(223, 'Aby zadeklarować pole klasy, do którego mają dostęp jedynie metody tej klasy i pole to nie jest dostępne dla klas pochodnych, należy użyć kwalifikatora dostępu', 'public', 'protected', 'private', 'published', 'C', 'PHP'),
(224, 'W języku PHP float reprezentuje typ', 'logiczny', 'łańcuchowy', 'całkowity', 'zmiennoprzecinkowy', 'D', 'PHP'),
(225, 'Instrukcja for może być zastąpiona instrukcją', ' continue', 'switch', 'case', 'while', 'D', 'Javascript'),
(226, 'DOM dostarcza metod i własności, które w języku JavaScript pozwalają na ', 'pobieranie i modyfikowanie elementów strony wyświetlonej przez przeglądarkę ', 'wykonywanie operacji na zmiennych przechowujących liczby ', 'manipulowanie zadeklarowanymi w kodzie łańcuchami ', 'wysłanie danych formularza bezpośrednio do bazy danych ', 'A', 'Javascript'),
(227, 'W relacyjnych bazach danych, jeżeli dwie tabele są połączone za pomocą ich kluczy głównych, mamy do czynienia z relacją', 'n..1', 'n..n', '1..1', '1..n', 'C', 'SQL'),
(228, 'Normalizacja tabel jest procesem, który ma na celu ', 'dodanie rekordów do bazy ', 'jedynie utworzenie tabel i relacji w bazie ', 'przedstawienie graficzne bazy ', 'sprawdzenie i optymalizację bazy danych ', 'D', 'SQL'),
(229, 'Wskaż zapytanie, w którym dane zostały posortowane.', 'SELECT imie, nazwisko FROM mieszkancy WHERE wiek > 18 ORDER BY wiek;', 'SELECT nazwisko FROM firma WHERE pensja > 2000 LIMIT 10;', 'SELECT AVG(ocena) FROM uczniowie WHERE klasa = 2;', 'SELECT DISTINCT produkt, cena FROM artykuly;', 'A', 'SQL'),
(230, 'W tabeli personel znajdują się pola: imie, nazwisko, pensja, staz. Aby otrzymać średnią pensję pracowników, dla których staż wynosi od 10 do 20 lat pracy włącznie, należy wykonać kwerendę: ', 'SELECT AVG(*) FROM personel WHERE staz >= 10 AND staz <= 20; ', 'SELECT COUNT(*) FROM personel WHERE staz >= 10 AND staz <= 20; ', 'SELECT AVG(pensja) FROM personel WHERE staz >= 10 AND staz <= 20; ', 'SELECT COUNT(pensja) FROM personel WHERE staz >= 10 AND staz <= 20; ', 'C', 'SQL'),
(231, 'Które tabele zostaną sprawdzone za pomocą przedstawionego polecenia?', 'Tabele, które zmieniły się od ostatniej kontroli lub nie zostały poprawnie zamknięte', 'Jedynie tabele, które nie zostały poprawnie zamknięte', 'Tabele, które zmieniły się w aktualnej sesji', 'Jedynie tabele referujące do innych', 'A', 'SQL'),
(233, 'Aby obraz wstawiony kodem HTML mógł być interpretowany przez programy wspomagające osoby niewidzące, należy zdefiniować atrybut', 'alt', 'border', 'sizes', 'src', 'A', 'HTML'),
(234, 'W jakim formacie należy zapisać obraz, aby mógł być wyświetlony na stronie internetowej z zachowaniem przezroczystości?', 'PNG', ' BMP', ' JPG', 'CDR', 'A', 'HTML');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `pytania`
--
ALTER TABLE `pytania`
  ADD PRIMARY KEY (`nr_pyt`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `pytania`
--
ALTER TABLE `pytania`
  MODIFY `nr_pyt` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=235;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
