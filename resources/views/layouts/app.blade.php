<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}" >
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="{{ asset('js/script.js') }}"></script>
        <title>{{config('app.name','test')}}</title>
    </head>
    <body>
        <div id='header'>
            Egzamin informatyk EE.09
         </div>
        <div id='center'>
            @yield('content')
        </div>
        <div id="footer">
            &copy; 2020 Jakub Wodarski <a href="https://gitlab.com/Kubart159/egzamin-ee.09" style="text-decoration:none;color:#d3d3d3;"><b> GitLab </b></a>
        </div>
    </body>
</html>
