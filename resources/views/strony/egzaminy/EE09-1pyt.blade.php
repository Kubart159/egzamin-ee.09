@extends('layouts.app')
@section('content')
    <div class='pyt_wrapper'>
        <?php

            $max = DB::table('pytania')->max('nr_pyt');
            do{
                $id = random_int(1,$max);
                $pytania = DB::table('pytania')->where('nr_pyt',$id)->first();
            }while(!isset($pytania));
            

            $trescPytania = $pytania->nr_pyt.". ".$pytania->pytanie;
        ?>
        <div class='pytanie'>
           {{$trescPytania}}
        </div>
        <p style="color:white;"> kategoria: {{$pytania->kategoria}} </p>
        <div class='odp_wrapper'>
            <div class="odp" id="A" onclick="wybor('A','{{$pytania->pop}}')">
                A. {{$pytania->odpA}}
            </div>
            <div class="odp" id="B" onclick="wybor('B','{{$pytania->pop}}')">
                B. {{$pytania->odpB}}
            </div>
            <div class="odp" id="C" onclick="wybor('C','{{$pytania->pop}}')">
                C. {{$pytania->odpC}}
            </div>
            <div class="odp" id="D" onclick="wybor('D','{{$pytania->pop}}')">
                D. {{$pytania->odpD}}
            </div>
        </div>
        <div id="nextButton" onclick="nastepne()">
            Następne pytanie
        </div>
    </div>
@endsection