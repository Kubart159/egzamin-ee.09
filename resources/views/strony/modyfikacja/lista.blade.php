@extends('layouts.app')
@section('content')
    <div id="lista_wrapper">
        <table id="lista_pytan">
            <tr>
                <th style="width:50px;"> Numer pytania </th>
                <th> Pytanie </th>
                <th style="width:100px;"> Odpowiedź A </th>
                <th style="width:100px;"> Odpowiedź B </th>
                <th style="width:100px;"> Odpowiedź C </th>
                <th style="width:100px;"> Odpowiedź D </th>
                <th style="width:100px;"> Odpowiedź poprawna </th>
                <th style="width:100px;"> Kategoria </th>
            </tr>
            <?php
            $pytaniaJSON = DB::table('pytania')->get();
            $pytania = json_decode($pytaniaJSON, true);

            foreach ($pytania as $pytanie) {
                echo '<tr><td style="text-align:center;">'.$pytanie['nr_pyt'].'</td><td>'.$pytanie['pytanie'].'</td><td>'.$pytanie['odpA'].'</td><td>'.$pytanie['odpB'].'</td><td>'.$pytanie['odpC'].'</td><td>'.$pytanie['odpD'].'</td><td style="text-align: center;">'.$pytanie['pop'].'</td><td>'.$pytanie['kategoria'].'</td></tr>';
                /*'<td>'.$pytanie['odpA'].'</td><td>'.$pytanie['odpB'].'</td><td>'.$pytanie['odpC'].'</td><td>'.$pytanie['odpD'].'</td><td>'.$pytanie['pop'].'</td><td>'.$pytanie['kategoria'].'</td></tr>'; */    
            }
            
            ?>            
            
        </table>
    </div>
@endsection
        