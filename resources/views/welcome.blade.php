@extends('layouts.app')
@section('content')
    <div id="welcome_wrapper">

        <a href="/40pyt">
            <div class="welcome_button" id="pyt40">
                40 pytań
            </div>
        </a>
        
        <a href="/1pyt">
            <div class="welcome_button" id="pyt1">
                Losowe pytanie
            </div>
        </a>

        <a href="/lista">
            <div class="welcome_button" id="lista">
                Lista pytań
            </div>
        </a>

        <a href="/dodaj">
            <div class="welcome_button" id="dodaj">
                Dodaj pytanie
            </div>
        </a>
        
    </div>
@endsection
        
