<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\KontrolerEgzaminow;

Route::get('/', function () {
    return view('welcome');
});

Route::get('40pyt','KontrolerEgzaminow@ee09_40');
Route::get('1pyt','KontrolerEgzaminow@ee09_1');

Route::get('dodaj','KontrolerModyfikacji@dodawanie');
Route::get('lista','KontrolerModyfikacji@lista');
Route::get('modyfikacja','KontrolerModyfikacji@modyfikacja');